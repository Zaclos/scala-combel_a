import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._

import models.finance_api._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ComputationSpec extends PlaySpec  {
  "bestStock" should {
    "return the smallest value" in {
      // ToDo write your tests
      bestStock(Nil) mustBe None
      risingStocks(Nil) mustBe None
	  fallingStocks(Nil) mustBe None
      meanPrice(Nil) mustBe None
      categoriesStocks(Nil) mustBe None
      deltasStocks(Nil) mustBe None
    }
  }
}