package models

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import play.api.libs.json._
import play.api.libs.ws._

import play.api.libs.json._
import play.api.libs.functional.syntax._

object finance_api {
  case class ResultsMetadata(
    `type`: String,
    start: Double,
    count: Double
  )
  case class Fields(
    change: BigDecimal,
    chg_percent: BigDecimal,
    day_high: BigDecimal,
    day_low: BigDecimal,
    issuer_name: String,
    issuer_name_lang: String,
    name: String,
    price: BigDecimal,
    symbol: String,
    ts: BigDecimal,
    `type`: String,
    utctime: String,
    volume: BigDecimal,
    year_high: BigDecimal,
    year_low: BigDecimal
  )
  case class Resource(
    classname: String,
    fields: Fields
  )
  case class Resources(
    resource: Resource
  )
  case class ResultsList(
    meta: ResultsMetadata,
    resources: List[Resources]
  )
  case class YahooResponse(
    list: ResultsList
  )

  case class Categories(
    `0-100`: Option[List[Fields]],
    `100-500`: Option[List[Fields]],
    `500-plus`: Option[List[Fields]]
    )

  case class YahooAnswer(
    bestStock: Option[Fields],
    risingStocks: Option[List[Fields]],
    fallingStocks: Option[List[Fields]],
    meanPrice: Option[BigDecimal],
    categories: Option[Categories],
    deltas: Option[Map[String, Map[String, BigDecimal]]]
    )

  case class Market(
    Symbol: String,
    Name: String,
    Exchange: String
    )

  // case class Market(
  //   field: MarketField
  //   )

  // case class Market(
  //   list: List[MarketField]
  //   )

  implicit val resultsMetadataFormat = Json.format[ResultsMetadata]
  implicit val fieldsFormat = Json.format[Fields]
  implicit val resourceFormat = Json.format[Resource]
  implicit val resourcesFormat = Json.format[Resources]
  implicit val resultsListFormat = Json.format[ResultsList]
  implicit val categoriesFormat = Json.format[Categories]
  
  implicit val yahooResponseFormat = Json.format[YahooResponse]
  implicit val yahooAnswerFormat = Json.format[YahooAnswer]

  //implicit val marketFieldFormat = Json.format[MarketField]
  implicit val marketFormat = Json.format[Market]
  //implicit val marketAnswerFormat = Json.format[MarketAnswer]


  def parseYahooResponse(js: JsValue): Option[YahooResponse] = js.validate[YahooResponse].asOpt

  def parseMarket(js: JsValue): Option[Market] = js.validate[Market].asOpt

  def getResults(stocks: List[String], ws: WSClient): Future[Option[YahooResponse]] = {
    val stockList = stocks.mkString(",")
    val request =
      ws.url("http://finance.yahoo.com/webservice/v1/symbols/" + stockList + "/quote")
        .withHeaders("Accept" -> "application/json")
        .withRequestTimeout(10000.millis)
        .withQueryString("format" -> "json", "view" -> "detail")

    val response = request.get()

    response map (_.json) map (parseYahooResponse _)
  }


  def getMarket(name: String, ws: WSClient): Future[Option[Market]] = {
    val request =
      ws.url("http://dev.markitondemand.com/MODApis/Api/v2/Lookup/json?input=" + name)
        .withHeaders("Accept" -> "application/json")
        .withQueryString("format" -> "json", "view" -> "detail")

    val response = request.get()
    response map (_.json) map (parseMarket _)
  }

  def getAnswer(stocks: List[Fields]): JsValue = {
    val answer = YahooAnswer(bestStock(stocks), risingStocks(stocks), fallingStocks(stocks),
                                 meanPrice(stocks), categoriesStocks(stocks), deltasStocks(stocks))
    Json.toJson(answer)
  }

  def getFields(yahooResponse: YahooResponse): List[Fields] = yahooResponse.list.resources.map(_.resource.fields)

  def bestStocks(stocks: List[Fields]): List[Fields] = stocks.sortBy(_.price).reverse
  
  def risingStocks(stocks: List[Fields]): Option[List[Fields]] = stocks match {
    case Nil => None
    case x::xs => Some((x::xs).filter(_.change > 0))
    }
  
  def fallingStocks(stocks: List[Fields]): Option[List[Fields]] = stocks match {
    case Nil => None
    case x::xs => Some((x::xs).filter(_.change <= 0))
    }

  def bestStock(stocks: List[Fields]): Option[Fields] = stocks match {
   case Nil     => None
   case x::xs   => Some((x::xs).maxBy(_.price))
  }

  def meanPrice(stocks: List[Fields]): Option[BigDecimal] = stocks match {
    case Nil    => None
    case x::xs  => Some((x::xs).map(_.price).sum / (x::xs).size)
  }

  def categoriesStocks(stocks: List[Fields]): Option[Categories] = 
  if (stocks.nonEmpty)
  {
    var order = stocks.groupBy(s => s.price >= 0 & s.price < 100)
    
    var c1 = order.getOrElse(true, null)
    if (c1 == null)
      c1 = List[Fields]()
    
    order = stocks.groupBy(s => s.price >= 100 & s.price < 500)
    
    var c2 = order.getOrElse(true, null)
    if (c2 == null)
      c2 = List[Fields]()
    
    order = stocks.groupBy(s => s.price >= 500)
    
    var c3 = order.getOrElse(true, null)
    if (c3 == null)
      c3 = List[Fields]()

    var categories = new Categories(Some(c1), Some(c2), Some(c3))
    Some(categories)
  }
  else
  {
    None
  }

  def deltasStocks(stocks: List[Fields]): Option[Map[String, Map[String, BigDecimal]]] = {
    if (stocks.isEmpty) {
      None
    } else {
      val deltas = stocks.map(x => ((x.name -> (stocks.map(y => (y.name -> (x.price - y.price))).toMap)))).toMap
      Some(deltas)
    }
  }

  // test pour utiliser groupBy

  // def groupStocksByChange(stocks: List[Fields]): List[Fields] = {
  //   var MapListFields = stocks.groupBy(_.change > 0)
  //   MapListFields.getOrElse(true, null)
  //   //MapListFields.getOrElse(false, null)
  // }

  // def groupStocksByName(stocks: List[Fields]): String = {
  //   var MapListFields = stocks.groupBy(_.name.charAt(0))
  //   MapListFields.toString
  // }
}

// -> Map [String, Map[String, BigDecimal]]
// -> case clase pour categories

// autres methodes
//def bestStock(stocks: List[Fields]): Option[Fields] = bestStocks(stocks).headOption
// autre methode
// def bestStock(stocks: List[Fields]): Option[Fields] = if (stocks.nonEmpty) {
//   Some(stocks.maxBy(_.price))
//   } else {
//     None
// }

// def deltasStocks(stocks: List[Fields]): Option[Map[String, Map[String, BigDecimal]]] = {
//     if (stocks.isEmpty) {
//       None
//     } else {
//           //style fonctionnel
//       val deltas = stocks.map(x => ((x.name -> (stocks.map(y => (y.name -> (x.price - y.price))).toMap)))).toMap
//       Some(deltas)
//     }
    //style imperatif
    // var deltas = Map[String, Map[String, BigDecimal]]()
    // var mapStock = Map[String, BigDecimal]()

    // for (stockSrc <- stocks)
    // {
    //   mapStock = Map[String, BigDecimal]()
    //   for (stockCmp <- stocks)
    //   {
    //     mapStock = mapStock + (stockCmp.name -> (stockSrc.price - stockCmp.price))
    //   }
    //   deltas = deltas + (stockSrc.name -> (mapStock));
    // }
  // }
