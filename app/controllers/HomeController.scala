package controllers

import javax.inject._
import scala.concurrent.ExecutionContext.Implicits.global
import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.ws._

import models.finance_api._

import play.api.mvc._
import play.api.Play.current

@Singleton
class HomeController @Inject() (ws: WSClient) extends Controller {

  def index = Action {
    Ok("hello world")
  }

  // pour info
  def info = Action.async { request =>
    val stocks = request.queryString.get("stock") match {
      case Some(seq) => seq.toList
      case None      => Nil
    }

    val data  = getResults(stocks, ws)

    def toResult(yahooResponse: YahooResponse): Result = {
      val fields = getFields(yahooResponse)
      Ok((getAnswer(fields)))
    }

    data map { result =>
      result map (toResult(_)) getOrElse InternalServerError
    }
  }

  //pour compagny
  def company(name: String) = Action.async { 
    val result = getMarket(name, ws)

    val data  = getMarket(name, ws)

    println(name)
    println(data)
    def rResult(market: Market): Result = {
      println("toto")
      Ok(Json.toJson(market))
    }

    data map { result =>
      result map (rResult(_)) getOrElse InternalServerError
    }
  }

  // pour webSocket
  //def socket = ???

  // def socket = WebSocket.using[String] {
  //   val out = Enumerator.imperative[String]()
  //   val in = Iteratee.foreach[String] {
  //     msg =>
  //       out.push(msg)
  //   }
  //   (in, out)
  // }

  // { request =>
  //     val stocks = request.queryString.get("stock") match {
  //     case Some(seq) => seq.toList
  //     case None      => Nil
  //   }

  //   val data  = getResults(stocks, ws)

  //   def toResult(yahooResponse: YahooResponse): Result = {
  //     val fields = getFields(yahooResponse)
  //     Ok((getAnswer(fields)))
  //   }

  //   data map { result =>
  //     result map (toResult(_)) getOrElse InternalServerError
  //   }
  // }

}
