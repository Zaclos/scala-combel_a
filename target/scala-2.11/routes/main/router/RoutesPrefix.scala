
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/Arnaud/scala-combel_a/conf/routes
// @DATE:Sat Mar 12 16:00:52 CET 2016


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
